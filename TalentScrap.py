"""
.. module:: TalentScrap Documentation
    :platform: Linux based
    :synopsis: This program can scrap `Talent Academy website <https://talentacademy.co.in/currentaffair.php>` and Download daily GK and current affairs as image file. It can also be converted using this program.
.. moduleauthor:: Athul R T

::This programme works on **Python 3.7** or later.
This python needs following modules installed in the system:
 * Os
 * errno
 * bs4
 * glob
 * img2pdf
 * calendar
 
Most of them mentioned here are present in the system by default. Others can be installed using ``pip`` or ``conda``.
"""
import os, errno
from bs4 import BeautifulSoup
import requests as req
import glob
import img2pdf
import calendar

class TalentScrap:
    """ Class that scraps Talent Academy Website.
    """
    def __init__(self):
        pass
        
    def img_downloader(self,ids):
        """ Func: img_downloader to download GK and Current Affairs files in ``.jpg`` format. Image file in ``.jpg`` format gets downloaded in the ``year/month/month_date.jpg`` format where year folder will be created within the same directory as that of code.
        
        :param ids:  The webpage id is given as input. It can be obtained from the web address ``https://talentacademy.co.in/currentaffair.php?id=XXX`` where *XXX* is the id here.
        :type ids: int
        """
        try:
            url = "https://talentacademy.co.in/currentaffair.php?id={}".format(ids)
            page = req.get(url)
            soup = BeautifulSoup(page.text,'lxml')
            talent = soup.find('div', attrs={'class':'post_meta'})
            textv = talent.text.split(" ")
            date = textv[5][0:2]
            month = textv[4]
            year = textv[6].strip("\n")
            imgs = soup.find('a', attrs={'class':'mfp-image'},href=True)
            img_link = imgs.get('href')
            r = req.get(img_link)
            try:
                os.makedirs(f'{year}/{month}')
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise
            try:
                file = open(f"{year}/{month}/{month}"+"_"+f"{date}.jpg", 'r')
            except FileNotFoundError:
                with open(f"{year}/{month}/{month}"+"_"+f"{date}.jpg", "wb") as f:
                    f.write(r.content)
                print(f"Downloaded {month}"+"_"+f"{date}.jpg")
        except (IndexError, AttributeError) as e:
            pass
    
    def batchdownload(self,start,stop):
        for i in range(start,stop+1):
            self.img_downloader(i)
    
    def pdfconvert(self,year):
        imagelist = sorted(glob.glob(f"{year}/**/*.jpg",recursive=True))
        monlist = glob.glob(f"{year}/*/")
        monthlist = [monlist[i].split("/")[1] for i in range(len(monlist)) ]
        for month in monthlist:
            with open(f"{year}/{month}.pdf", "wb") as f:
                f.write(img2pdf.convert([image for image in imagelist]))
        return print(f"Files created in {year} folder")
